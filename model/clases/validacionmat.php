<?php

require_once 'conexion.php';

class Validaciones{

	private $objPdo;
	private $codigo_val;
	private $codigo_alu;
	private $codigo_sem;
	private $carnet_val;
	private $idiomas_val;
	private $usuario_val;
	private $fecharegistro_val;

	public function __construct($codigo_val="",$codigo_alu="",$codigo_sem="",$carnet_val="",$idiomas_val="",$usuario_val="",$fecharegistro_val=""){

		$this->codigo_val = $codigo_val;
		$this->codigo_alu = $codigo_alu;
		$this->codigo_sem = $codigo_sem;
		$this->carnet_val = $carnet_val;
		$this->idiomas_val = $idiomas_val;
		$this->usuario_val = $usuario_val;
		$this->fecharegistro_val = $fecharegistro_val;
        $this->objPdo = new Conexion();
	}

	public function listaaluparacarnet($semestre,$semestre1,$semestre2){
        $sql = "SELECT C1.Codigo_Alu,C1.estudiante,C1.Nombre_Esc,c1.Codigo_Sem,(CASE WHEN C2.carnet_val is null THEN 0 ELSE c2.carnet_val END) as estado FROM (SELECT a.codigo_Alu,(a.ApellidoPat_Alu+' '+a.ApellidoMat_Alu+' '+a.nombres_alu) as 'estudiante',e.Nombre_Esc,s.Codigo_Sem from Alumno a inner join (SELECT distinct a.codigo_Alu,m.identificador_Alu,m.identificador_Mat,ap.montoAPagar_Apo,ap.montoPagado_Apo,ap.estado_Apo FROM DBPAGOS.dbo.Matricula m inner join DBPAGOS.dbo.Alumno a on m.identificador_Alu=a.identificador_Alu inner join DBPAGOS.dbo.Aporte ap on m.identificador_Mat=ap.identificador_Mat inner join DBPAGOS.dbo.Pago p on p.codigo_Peri=m.codigo_Peri and p.identificador_Pag=ap.identificador_Pag where m.codigo_Peri=(SELECT codigo_Peri FROM DBPAGOS.dbo.Periodo where nombre_semestre COLLATE Modern_Spanish_CI_AS=(select Nombre_Sem from Semestre where Codigo_Sem=:semestre) COLLATE Modern_Spanish_CI_AS) and p.tipo_Pag='M' and ap.estado_Apo='PA')b on a.codigo_Alu COLLATE Modern_Spanish_CI_AS=b.codigo_Alu COLLATE Modern_Spanish_CI_AS INNER JOIN Escuela E ON e.Codigo_Esc=a.Codigo_Esc cross join Semestre s where s.Codigo_Sem=:semestre1)C1 LEFT JOIN (SELECT codigo_alu,codigo_sem,carnet_val from matricula.validaciones where codigo_sem=:semestre2 and carnet_val>0)C2 ON C1.Codigo_Alu=C2.codigo_alu and C1.Codigo_Sem=C2.codigo_sem";
        $stmt=$this->objPdo->prepare($sql);
        $stmt->execute(array('semestre'=>$semestre,'semestre1'=>$semestre1,'semestre2'=>$semestre2));
        $lalucarvig = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $lalucarvig;
    }

    public function listaaluparaidiomas($semestre,$semestre1,$semestre2){
        $sql = "SELECT C1.Codigo_Alu,C1.estudiante,C1.Nombre_Esc,c1.Codigo_Sem,(CASE WHEN C2.idiomas_val is null THEN 0 ELSE C2.idiomas_val END) as estado FROM (SELECT a.codigo_Alu,(a.ApellidoPat_Alu+' '+a.ApellidoMat_Alu+' '+a.nombres_alu) as 'estudiante',e.Nombre_Esc,s.Codigo_Sem from Alumno a inner join (SELECT distinct a.codigo_Alu,m.identificador_Alu,m.identificador_Mat,ap.montoAPagar_Apo,ap.montoPagado_Apo,ap.estado_Apo FROM DBPAGOS.dbo.Matricula m inner join DBPAGOS.dbo.Alumno a on m.identificador_Alu=a.identificador_Alu inner join DBPAGOS.dbo.Aporte ap on m.identificador_Mat=ap.identificador_Mat inner join DBPAGOS.dbo.Pago p on p.codigo_Peri=m.codigo_Peri and p.identificador_Pag=ap.identificador_Pag where m.codigo_Peri=(SELECT codigo_Peri FROM DBPAGOS.dbo.Periodo where nombre_semestre COLLATE Modern_Spanish_CI_AS=(select Nombre_Sem from Semestre where Codigo_Sem=:semestre) COLLATE Modern_Spanish_CI_AS) and p.tipo_Pag='M' and ap.estado_Apo='PA')b on a.codigo_Alu COLLATE Modern_Spanish_CI_AS=b.codigo_Alu COLLATE Modern_Spanish_CI_AS INNER JOIN Escuela E ON e.Codigo_Esc=a.Codigo_Esc
			cross join Semestre s where s.Codigo_Sem=:semestre1)C1 LEFT JOIN (SELECT codigo_alu,codigo_sem,idiomas_val from matricula.validaciones where codigo_sem=:semestre2 and idiomas_val>0)C2 ON C1.Codigo_Alu=C2.codigo_alu and C1.Codigo_Sem=C2.codigo_sem";
        $stmt=$this->objPdo->prepare($sql);
        $stmt->execute(array('semestre'=>$semestre,'semestre1'=>$semestre1,'semestre2'=>$semestre2));
        $laluidioma = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $laluidioma;
    }

    public function insertar(){
        $stmt =$this->objPdo->prepare("INSERT INTO matricula.Validaciones(codigo_alu,codigo_sem,usuario_val,fecharegistro_val) values(:codigo_alu,:codigo_sem,:usuario_val,:fecharegistro_val);");
        $stmt->execute(array('codigo_alu'=>$this->codigo_alu,
                             'codigo_sem'=>$this->codigo_sem,
                             'usuario_val'=>$this->usuario_val,
                             'fecharegistro_val'=>$this->fecharegistro_val));
    }

    public function modificarcarnet(){
        $stmt = $this ->objPdo->prepare("UPDATE matricula.Validaciones SET carnet_val=:carnet_val
                                         WHERE codigo_alu=:codigo_alu and codigo_sem=:codigo_sem;");
        $rows = $stmt->execute(array('carnet_val'=>$this->carnet_val,
                                     'codigo_alu'=>$this->codigo_alu,       
                                     'codigo_sem'=>$this->codigo_sem));
    }

    public function modificaridiomas(){
        $stmt = $this ->objPdo->prepare("UPDATE matricula.Validaciones SET idiomas_val=:idiomas_val
                                         WHERE codigo_alu=:codigo_alu and codigo_sem=:codigo_sem;");
        $rows = $stmt->execute(array('idiomas_val'=>$this->idiomas_val,
                                     'codigo_alu'=>$this->codigo_alu,       
                                     'codigo_sem'=>$this->codigo_sem));
    }

    public function verificarregistro($alumno,$semestre){
        $sql = "SELECT * from matricula.Validaciones where codigo_alu=:alumno and codigo_sem=:semestre";
        $stmt=$this->objPdo->prepare($sql);
        $stmt->execute(array('alumno'=>$alumno,'semestre'=>$semestre));
        $lregistros = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $lregistros;
    }

	public function getcodigoval() {
        return $this->codigo_val;
    }
    public function setcodigoval($codigo_val){
        $this->codigo_val = $codigo_val;
    }

    public function getcodigoalu() {
        return $this->codigo_alu;
    }
    public function setcodigoalu($codigo_alu){
        $this->codigo_alu = $codigo_alu;
    }

    public function getcodigosem() {
        return $this->codigo_sem;
    }
    public function setcodigosem($codigo_sem){
        $this->codigo_sem = $codigo_sem;
    }

    public function getcarnetval() {
        return $this->carnet_val;
    }
    public function setcarnetval($carnet_val){
        $this->carnet_val = $carnet_val;
    }

    public function getidiomasval() {
        return $this->idiomas_val;
    }
    public function setidiomasval($idiomas_val){
        $this->idiomas_val = $idiomas_val;
    }

    public function getusuarioval() {
        return $this->usuario_val;
    }
    public function setusuarioval($usuario_val){
        $this->usuario_val = $usuario_val;
    }

    public function getfecharegval() {
        return $this->fecharegistro_val;
    }
    public function setfecharegval($fecharegistro_val){
        $this->fecharegistro_val = $fecharegistro_val;
    }


}



?>