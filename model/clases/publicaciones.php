<?php

#require_once 'conexion.php';

class publicaciones{

	private $objPdo;
    private $codigo_evento;
    private $titulo_evento;
    private $descripcion_evento;
    private $fecha_evento;
    private $categoria_evento;
    private $foto_evento;



    function registrarevento() {
     
     $titulo=  ($_POST["titulo"]);
     $descripcion= ($_POST["descrevento"]);
     $categoria= ($_POST["categoriaevento"]);
     $link=  ($_POST["linkfoto"]);
     $usuario= $_SESSION['usuario'];
     $id= $_SESSION['id_usuario'];
     $nombreDestino=$_SESSION['nombre_destino'];
     $idDestino= $_SESSION['id_destino']; 
     $fecha= ($_POST["fechaevento"]); 
     $horaev= ($_POST["hora"]);   
     $nuevoformato=date_create($fecha);
     $formato=date_format($nuevoformato,'Y-m-d');
     $t= "T";
     //substr(, start)
     $fechaev= $formato.$t.$horaev.':00.00+0000';
     $fechahora= $fechaev;
  

     //2019-07-07T14:00:16.672+0000

            $apiUrl = 'http://muchikapi.herokuapp.com/api/novedad/guardar/';
            $evento = array(
                    'idDestino' => $idDestino,
                    'nombreDestino' => $nombreDestino,
                    'idUsuario' => $id,
                    'nombreUsuario' => $usuario,
                    'titulo' => $titulo,
                    'descripcion' =>$descripcion,
                    //'fecha' => '2019-07-07T14:00:16.672+0000',
                    'fecha' => $fechahora,
                    'foto' => $link,
                    'tipo' => 'E',
                    'categoriaEvento' => $categoria,
                    'vigencia' => true
                );
            $body_json = json_encode($evento); 
            $curl = curl_init($apiUrl);
            curl_setopt($curl, CURLOPT_ENCODING ,"");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body_json);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($body_json))                                                                       
            ); 
            $json = curl_exec($curl);
           // print($json);
            curl_close($curl);
            $data = json_decode($json, true);
            return $data;

 }


 function registrarnoticia()

 {

     $titulo=  ($_POST["titulonot"]);
     $descripcion= ($_POST["descripcionnot"]);
     $foto=  ($_POST["linkfoto"]);
     $usuario= $_SESSION['usuario'];
     $id= $_SESSION['id_usuario'];
     $nombreDestino=$_SESSION['nombre_destino'];
     $idDestino= $_SESSION['id_destino'];
     $fecha=$_POST["fechanoticia"]; 



            $apiUrl = 'http://muchikapi.herokuapp.com/api/novedad/guardar/';
            $body = array(
                    'idDestino' => $idDestino,
                    'nombreDestino' => $nombreDestino,
                    'idUsuario' => $id,
                    'nombreUsuario' => $usuario,
                    'titulo' => $titulo,
                    'descripcion' => $descripcion,
                    'fecha' => $fecha,
                    'foto' => $foto,
                    'tipo' => 'N',
                    'vigencia' => true
                );
            $body_json = json_encode($body); 
            $curl = curl_init($apiUrl);
            curl_setopt($curl, CURLOPT_ENCODING ,"");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body_json);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($body_json))                                                                       
            ); 
            $json = curl_exec($curl);
            //print($json);
            curl_close($curl); 
            $data = json_decode($json, true);
            
            return $data;
 } 

 function listarevento($user){
           // $correo = '$user';
            $apiUrl = 'http://muchikapi.herokuapp.com/api/novedad/eventos/usuario/';
            $parametros=$user;
            $curl = curl_init($apiUrl . $parametros);
            curl_setopt($curl, CURLOPT_ENCODING ,"");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPGET, true);
            $json = curl_exec($curl);
           # print($json);
            curl_close($curl);
            $data = json_decode($json, true);
          
            return $data;

    }
 function listarnoticia($user){
           // $correo = '$user';
            $apiUrl = 'http://muchikapi.herokuapp.com/api/novedad/noticias/usuario/';
            $parametros=$user;
            $curl = curl_init($apiUrl . $parametros);
            curl_setopt($curl, CURLOPT_ENCODING ,"");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPGET, true);
            $json = curl_exec($curl);
           # print($json);
            curl_close($curl);
            $data = json_decode($json, true);
          
            return $data;

    }



}


?>