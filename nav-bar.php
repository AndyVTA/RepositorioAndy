<?php 

#require_once 'model/clases/menunav.php';

#$menunav = new MenuNav();
#$menunavs = $menunav->getmenuxresp(($_SESSION['idperfil']));
#?>

<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
				
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<a class="btn btn-small btn-success opcion1" title="Dashboard">
							<i class="ace-icon fa fa-signal"></i>
						</a>

						<a class="btn btn-small btn-info opcion2" title="Bitacora" href="#" >
							<i class="ace-icon fa fa-eye"></i>
						</a>

						<a class="btn btn-small btn-warning opcion3" href="index.php?page=usuarios&accion=listar" title="Usuarios">
							<i class="ace-icon fa fa-users"></i>
						</a>

						<a class="btn btn-small btn-danger opcion4" href="#" title="Configuración">
							<i class="ace-icon fa fa-cogs"></i>
						</a>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->
			
				<ul class="nav nav-list">
					<li class="active">
						<a href="index.php?page=login&accion=entrar">
							<i class="menu-icon fa fa-home"></i>
							<span class="menu-text"> Inicio </span>
						</a>

						
					</li>
				
					<li>
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-book"></i>
							<span class="menu-text">Publicaciones  </span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<ul class="submenu">
			

							<li class=""> 
								<a href="index.php?page=publicaciones&accion=listarevento"><i class="menu-icon fa fa-caret-right"></i>Eventos</a>
								<b class="arrow"></b>
							</li>

							<li class=""> 
								<a href="index.php?page=publicaciones&accion=listarnoticia"><i class="menu-icon fa fa-caret-right"></i>Noticias</a>
								<b class="arrow"></b>
							</li>

							
						</ul>
					</li>

					<li>
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-eye"></i>
							<span class="menu-text">Reportes</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<ul class="submenu">
			

							<li class=""> 
								<a href="index.php?page=publicaciones&accion=listartodo"><i class="menu-icon fa fa-caret-right"></i>Informes</a>
								<b class="arrow"></b>
							</li>


							
						</ul>
					</li>
				

				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
</div>



