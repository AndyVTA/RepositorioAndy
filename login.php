<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <meta name="description" content="Sistema de Matricula" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <title>Sistema de Gestión Publicaciones</title>


        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

       
        <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="assets/css/ace.min.css" />

        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />


        <!--[if IE 7]>
          <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
        <![endif]-->

        <!--page specific plugin styles-->

        
        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="shortcut icon" href="view/img/logo.ico">

    </head>

    <body class="login-layout">

        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1 class="white">
                                   <!--<img src="view/img/login1.png" style="margin-left:-20px;">!-->
                                    <!--<span class="white">SISTEMA DE MATRICULA</span>-->
                                    <!-- <span class="white">Application</span> -->
                                    SISTEMA DE GESTIÓN DE PUBLICACIONES
                                </h1>
                                                      
                                 <h4 class="white"></h4>
                                 <h5 class="white">&copy; ADMININISTRADOR SISTEMAS</h5>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <!-- <i class="ace-icon fa fa-coffee green"></i> -->
                                                Por favor Ingresar sus Datos
                                            </h4>

                                            <div class="space-6"></div>

                                            <form id="form-login" action="index.php?page=login&accion=login" method="POST">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <label>Nombre de Usuario</label>

                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" autocomplete="off" name="user" id="user" autofocus required class="form-control" placeholder="Usuario" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <label> Contraseña</label>
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" name="pass" id="pass" required class="form-control" placeholder="Contraseña" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <div class="clearfix">
                                                       
                                                        <button id="submit-login" class="width-100 pull-right btn btn-small btn-primary">
                                                            <i class="ace-icon fa fa-sign-in icon-on-left bigger-110"></i>
                                                            <span class="bigger-110">Entrar</span>
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /.widget-main -->

                                        
                                    </div><!-- /.widget-body -->
                                </div><!-- /.login-box -->

                                

                                
                            </div><!-- /.position-relative -->

                            
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.main-content -->
        </div><!-- /.main-container -->


                    
                        
                            
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>  


        <script type="text/javascript">
                    window.jQuery || document.write("<script src='assets/js/jquery-2.1.4.min.js'>"+"<"+"/script>");
        </script>

        <script src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script> 
        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
        <script src="view/js/reglas.js"></script> 
    </body>
</html>
