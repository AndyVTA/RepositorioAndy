<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<title>Sistema de Publicaciones</title>

		<meta name="description" content="Common UI Features &amp; Elements" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!--basic styles-->

		<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/font-awesome-4.3.0/css/font-awesome.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="assets/css/jquery.gritter.css" />
		<link rel="stylesheet" href="assets/css/smoothslides.theme.css">

		<!--fonts-->

		<link rel="stylesheet" href="js/autosource/googleapis.css" />

		<!--ace styles-->

		<link rel="stylesheet" href="assets/css/ace.min.css" />
		<link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />

		<script src="assets/js/jquery.mobile-1.4.5.min.js"></script>
		<link rel="stylesheet" href="css/personalizada.css" />
		<link rel="shortcut icon" href="assets/img/micros.ico">
		
		<!--inline styles related to this page-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
    
        <script src="assets/js/jquery-1.10.2.min.js"></script> 

		
		<style type="text/css">
				    #img_logo{
				        max-width: 330px;
				        margin-left:  -70px;

				    }
    	</style>

<script type="text/javascript">
	$(document).live("orientationchange", function() {
		alert("cambio de orientacion");
	} );
</script>
    </head>

	<body onload="openDialog();">

			

			<?php
				include 'barrasesion.php'
			
			?>

		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>

			<?php
				include 'nav-bar.php'
			
			?>
			
			<div class="main-content">

					<div class="breadcrumbs" id="breadcrumbs">
							<ul class="breadcrumb">
								<li>
									<i class="icon-user-md home-icon"></i>
									<a href="#">UMB BIBLIOTECA</a>

									<span class="divider">
										<i class="icon-angle-right arrow-icon"></i>
									</span>
								</li>
								<li class="active">Inicio</li>
							</ul><!--.breadcrumb-->
							<!-- 
							<div class="nav-search" id="nav-search">
								<form class="form-search" />
									<span class="input-icon">
										<input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
										<i class="icon-search nav-search-icon"></i>
									</span>
								</form>
							</div> --><!--#nav-search-->
					</div>

				<!-- Contenido-pag-->
				<div class="page-content">
                 

                  <!-- Sub migajas

					<div class="page-header position-relative">
						<h1><small>
								<i class="icon-double-angle-right"></i>
								overview &amp; stats
							</small>
						</h1>
					</div>

					-->
					<div class="row-fluid">
						
						<!--<div class="span12"> -->



							<div class="row-fluid">	
								<h2 class="text-info" style="text-align:center">Bienvenido al Sistema de Gestion de Biopsias</h2>
					            <h4 class="text-info" style="text-align:center">Hospital Regional Lambayeque</h4>
					          	<!--<div class="ss-slides" >
								    <div class="ss-slide" title="Patologia Quirugica">
								        <img  src="view/img/pq.jpg"/>
								    </div>
								    <div class="ss-slide" title="Citologia Especial">
								        <img  src="view/img/ce.jpg" />
								    </div>
								    <div class="ss-slide" title="Citologia Cervico Vaginal">
								        <img  src="view/img/ccv.jpg" />
								    </div>
								    <div class="ss-slide" title="Inmunohistoquimica">
								        <img  src="view/img/ih.jpg" />
								    </div>
								    								    
								</div>-->
					        
							
						</div>
					</div>
					
				 </div><!-- fin contenido-->

		
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-mini btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="fa fa-book bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-class="default" value="#438EB9" />#438EB9
									<option data-class="skin-1" value="#222A2D" />#222A2D
									<option data-class="skin-2" value="#C6487E" />#C6487E
									<option data-class="skin-3" value="#D0D0D0" />#D0D0D0
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div>
							<input type="checkbox" class="ace-checkbox-2" id="ace-settings-header" />
							<label class="lbl" for="ace-settings-header"> Fixed Header</label>
						</div>

						<div>
							<input type="checkbox" class="ace-checkbox-2" id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div>
							<input type="checkbox" class="ace-checkbox-2" id="ace-settings-breadcrumbs" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div>
							<input type="checkbox" class="ace-checkbox-2" id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>
					</div>
				</div><!--/#ace-settings-container-->			

		   </div><!--/.main-content-->
		</div><!--/.main-container-->



		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small ">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--SCRIPTS PROYECTO-->
	<script src="assets/js/jquery.min.js"></script>

		<!--<![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="assets/js/jquery.js"></script>
		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/spin.min.js"></script>
		<script src="assets/js/smoothslides.min.js"></script>

		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>
		<script type="text/javascript">
		/* wait for images to load */
		$(window).load( function() {
			$(document).smoothSlides({
			duration: 5000
			/* options seperated by commas */
			});
		});
	</script>




	</body>
</html>
