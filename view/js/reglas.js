/*--------------------------------------
Evitar el Control
--------------------------------------*/
document.onkeydown = function(e) {
    if ((e.ctrlKey && e.keyCode === 85) || //Control + U
        (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) //Control + Shift + I
        || (e.keyCode === 123)) //F12
    {            
        return false;
    } else {
        return true;
    }

   
};

/*--------------------------------------
Evitar Anti Clic
--------------------------------------*/
document.oncontextmenu = function(){return false;}
