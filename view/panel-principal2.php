<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Sistema de Gestion de Publicaciones</title>

		<meta name="description" content="Sistema de Matricula" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!--basic styles-->

		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/introjs.css">
		<link rel="stylesheet" href="assets/css/smoothslides.theme.css">

		<script src="assets/js/ace-extra.min.js"></script>
        <script src="assets/js/jquery-1.11.3.min.js"></script> 

		
		<style type="text/css">
				    #img_logo{
				        max-width: 330px;
				        margin-left:  -70px;

				    }
    	</style>

		<script type="text/javascript">
			$(document).live("orientationchange", function() {
				alert("cambio de orientacion");
			} );
		</script>
    </head>

	<body class="no-skin">

			

			<?php
				include 'barrasesion.php'
			
			?>

		<div class="main-container ace-save-state" id="main-container">
			<!-- <a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a> -->
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<?php
				include 'nav-bar.php'
			
			?>
			
			<div class="main-content">

					<div class="breadcrumbs" id="breadcrumbs">
							<ul class="breadcrumb">
								<li>
									<i class="icon-home bigger-120"></i>
									<a href="index.php?page=login&accion=entrar">SISTEMA DE GESTIÓN PUBLICACIONES</a>

									<span class="divider">
										<i class="icon-angle-right arrow-icon"></i>
									</span>
								</li>
								<li class="active">Inicio</li>
							</ul><!--.breadcrumb-->
							
							<!--
							<div class="nav-search" id="nav-search">
								<form class="form-search" />
									<?php if($_SESSION['idperfil']==1):?>
									<a id="btnayuda" class="label label-large label-pink arrowed-right">Guia de Usuario
									</a>
									<?php endif?>
									<?php if($_SESSION['idperfil']!=1):?>
									 <a id="btnayudausuario" class="label label-large label-pink arrowed-right">Guia de Usuario
									 </a>
									<?php endif?>	
								</form>
							</div> -->
					</div>

				<!-- Contenido-pag-->
				<div class="page-content">
                 

                  <!-- Sub migajas

					<div class="page-header position-relative">
						<h1><small>
								<i class="icon-double-angle-right"></i>
								overview &amp; stats
							</small>
						</h1>
					</div>

					-->
					<div class="row-fluid">
						
						<div class="span12">

							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<i class="icon-ok green"></i>
					            Bienvenido al Sistema de Gestión de Publiaciones (V1.0), para cualquier informaci&oacute;n favor de comunicarse con los administradores del sistemas
					            					            
					            <!-- Unidad de Desarrollo de Sistemas.   -->

					            
					           
					            <!-- <button type="button" class="btn btn-warning" style="float: right;margin-top: -4%;" id="btnayuda" name="btnayuda"> Ayuda</button> -->
					        </div>

						<div class="span2">
							
						</div>
							<div class="row-fluid span8 text-center">	
								
					          	<div class="ss-slides" >
								    <div class="ss-slide" title="Tumbas Reales">
								        <img  src="view/img/4.jpg"/>
								    </div>
								    <!-- <div class="ss-slide" >
								        <img  src="assets/img/biblioteca.png" />
								    </div> -->
								    <!--<div class="ss-slide" title="Citologia Cervico Vaginal">
								        <img  src="view/img/ccv.jpg" />
								    </div>
								    <div class="ss-slide" title="Inmunohistoquimica">
								        <img  src="view/img/ih.jpg" />
								    </div> -->
								    								    
								</div>
					      </div> 
					      <div class="row-fluid span4">
							
						</div>
			
						</div>
					</div>
					
				 </div><!-- fin contenido-->

								

		   </div><!--/.main-content-->
		</div><!--/.main-container-->



		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small ">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>

		<!--SCRIPTS PROYECTO-->
		<script src="assets/js/jquery-2.1.4.min.js"></script>

		
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="assets/js/jquery-ui.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.easypiechart.min.js"></script>
		<script src="assets/js/jquery.sparkline.index.min.js"></script>
		<script src="assets/js/jquery.flot.min.js"></script>
		<script src="assets/js/jquery.flot.pie.min.js"></script>
		<script src="assets/js/jquery.flot.resize.min.js"></script>
		<script src="assets/js/smoothslides.min.js"></script>
		<script src="assets/js/intro.js"></script>
		<script src="view/js/guiaprincipal.js"></script>
		<script src="view/js/reglas.js"></script> 
		<!--ace scripts-->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>
		<script type="text/javascript">
		/* wait for images to load */
		$(window).load( function() {
			$(document).smoothSlides({
			duration: 5000
			/* options seperated by commas */
			});
		});
	</script>




	</body>
</html>
