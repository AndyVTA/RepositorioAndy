<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <title>Registro de Noticia</title>
        
         
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/introjs.css">
        
        <script src="assets/js/ace-extra.min.js"></script>
        <script src="assets/js/jquery-1.11.3.min.js"></script> 
       
        
    </head>
    <body class="no-skin">
        <?php
        include 'barrasesion.php';
        ?>




        <div class="main-container ace-save-state" id="main-container">
             <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>
            <?php
    
                    include 'nav-bar.php';
      
            ?>
            <div class="main-content">
                <div class="breadcrumbs" id="breadcrumbs">
                        <ul class="breadcrumb">
                              <li>
                                <i class="icon-wrench home-icon"></i>
                                <a href="index.php?page=login&accion=listarnoticia">Mantenimiento</a>

                                <span class="divider">
                                    <i class="icon-angle-right arrow-icon"></i>
                                </span>
                            </li>
                          
                            <li class="active">Noticias</li>
                        </ul><!--.breadcrumb-->
                        <!-- 
                        <div class="nav-search" id="nav-search">
                            <form class="form-search" />
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
                                    <i class="icon-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div> --><!--#nav-search-->
                    </div>


               <div class="page-content">
                  
                      <div class="row-fluid">
                        <div class="col-xs-12">
                              
                         <h2 class="header smaller lighter blue">Registro Noticias</h2>



                         <div class="widget-box">
                             <div class="widget-header">
                                <h4>Datos</h4>
                                
                             </div><!-- widget-header -->
                             <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row-fluid">
                                        <div class="space-6"></div>
                                 
                                        <form  class="form-horizontal " action="index.php?page=publicaciones&accion=insertarnoticia" method="POST">
                                            <div class="col-sm-12">

                                                <div class="col-sm-12">

                                                   <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Titulo</label>
                                                       <div class="col-sm-10">
                                                            <input type="text" required="required" class="col-xs-10 col-sm-12" name="titulonot" id="titulonot"  value=""  autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Descripción</label>
                                                       <div class="col-sm-10">
                                                            <textarea   rows="3" required="required" class="col-xs-10 col-sm-12" name="descripcionnot" id="descripcionnot"  value="" autocomplete="off"> 
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                     <div class="form-group" hidden>
                                                        <label class="col-sm-1 control-label no-padding-right" >Fecha</label>
                                                       <div class="col-sm-10">
                                                            <input type="text" required="required" class="col-xs-10 col-sm-12" name="fechanoticia" id="fechanoticia"  value="<?php ;echo date("Y-m-d"); ?>"  autocomplete="off">
                                                        </div>
                                                    </div>



                                                        <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Link Foto</label>
                                                       <div class="col-sm-10">
                                                            <input type="url" required="required" class="col-xs-10 col-sm-12" name="linkfoto" id="linkfoto"  value="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                        <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Imagen</label>   <center><img  src="" id="img1" alt="debe ingresar el link" height="180px" width="400px" /></center>
                                                   
                                                    </div>






                                                </div>
                                                
                     
                                            </div> 
                                            <div class="space-6"></div>
                                            <div class=" text-center">
                                                <button type="submit" class="btn btn-primary"> <i class="fa fa-floppy-o icon-on-left bigger-110"></i> Guardar</button>
                                                 <a href="index.php?page=publicaciones&accion=listarnoticia" class="btn btn-danger"><i class="fa fa-times-circle icon-on-left bigger-110"></i> Cancelar</a>
                                                
                                            </div>

                                        </form>

                                    </div><!-- row-fluid -->
                                    
                                </div><!-- widget-main -->
                                 
                             </div><!-- widget-body -->
                         </div><!-- widget-box -->
                            
            


                        </div><!-- span12 -->

                      </div><!-- row-fluid -->
                     
               </div> <!-- page-content -->


            </div><!-- main-content -->


        </div><!-- main-container -->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small ">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>

       <script src="assets/js/jquery-2.1.4.min.js"></script>
       
         <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>

        <!--page specific plugin scripts-->

        <!--[if lte IE 8]>
          <script src="assets/js/excanvas.min.js"></script>
        <![endif]-->
        <script src="assets/js/jquery-ui.custom.min.js"></script>
        <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
        
        <script src="assets/js/bootbox.min.js"></script>
        

        <!--ace scripts-->

        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
        <script src="assets/js/bootbox.js"></script> 
        <script src="view/js/noticia.js"></script>

    </body>
</html>
