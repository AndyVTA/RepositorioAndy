<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Noticias</title>
 
        <meta name="description" content="Sistema de Matricula" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/introjs.css">
        
        <script src="assets/js/ace-extra.min.js"></script>
        <script src="assets/js/jquery-1.11.3.min.js"></script> 

        <style type="text/css">
        .modalesperando {
                display:    none;
                position:   fixed;
                z-index:    50;
                top:        0;
                left:       0;
                height:     100%;
                width:      100%;
                background: rgba( 255, 255, 255, .8 ) 
                            url('assets/images/39.GIF') 
                            50% 50% 
                            no-repeat;
            }
        </style>

                    
        <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
        $('#listanoticias').dataTable();
            } );
        </script>


    </head>
    <body class="no-skin">
        <?php
        // put your code here
        include 'barrasesion.php';
        ?>

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>

            <?php
            // put your code here
            include 'nav-bar.php';
           
            ?>
 
        <div class="main-content">

                <div class="breadcrumbs" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-wrench home-icon"></i>
                            <a href="#">Publicaciones</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li> 
                        <li class="active">Noticias</li>
                    </ul><!--.breadcrumb-->
                    <!-- 
                    <div class="nav-search" id="nav-search">
                        <form class="form-search" />
                            <span class="input-icon">
                                <input type="text" placeholder="Search ..." class="input-small nav-search-input" id="nav-search-input" autocomplete="off" />
                                <i class="icon-search nav-search-icon"></i>
                            </span>
                        </form>
                    </div> --><!--#nav-search-->
                </div>

            <div class="page-content">
                <div class="row-fluid">
                    <div class="col-xs-12">
                   <!--      <div class="row-fluid"> -->
                            
                            <h2 class="header smaller lighter blue">Noticias</h2>
                            <div class="col-xs-12">
                                          <div class="col-xs-10">
                                            <h5 class="text-success">Cantidad de registros <span class="badge"><?php echo count($listnoticia); ?> </span></h5>
                                        </div>
                                         <div class="col-xs-2">
                                            <a class="btn btn-success" href="index.php?page=publicaciones&accion=registrarnoticia"><i class="fa fa-file-o icon-on-left bigger-110"></i> Nuevo</a>
                                        </div>
                           </div>
                        <br><br><br>

                            <div class="table-responsive">
                                <div class="table-header">
                                    Noticias registradas en el sistema.
                                </div>
                                <table id="listanoticias" class="table table-striped table-bordered table-hover dataTable dt-responsive"  cellspacing="0" width="100%">

                                     <thead>
                                        <tr >
                                            <th class="center">CODIGO</th>
                                            <th class="center">TITULO</th>
                                            <th class="center">DESCRIPCION</th> 
                                            <th class="center">FOTO</th>
                                            <th class="center">FECHA</th>   
                                                                         
                                            <th class="center">ACCION</th>
                                            
                                        </tr>
                                     </thead>
                                    <tbody>
                                           <?php if (count($listnoticia)): ?>
                                            <?php foreach ($listnoticia as $noticia): ?>
                                                <tr>
                                                    <td><?php echo substr($noticia["id"],0,10)."...";?></td>
                                                    <td><?php echo substr($noticia["titulo"],0,30)."...";?></td>
                                                    <td><?php echo substr($noticia["descripcion"],0,25)."..."; ?></td>
                                                    <td><?php echo substr($noticia["foto"], 0,14)."..."; ?></td>
                                                     <td><?php echo substr($noticia["fecha"],0,10);?></td>
                                                   
                                                    <td class="td-actions">
                                                         
                                                        <div class="action-buttons text-center">
                                                           <a class="blue" title="Modificar" href="">
                                                                <i class="fa fa-pencil bigger-180"></i>
                                                            </a>
                                                            <a class="red btn-delete" title="Eliminar" id="eliminar">
                                                                <i class="fa fa-trash bigger-180"></i>
                                                            </a>
                                                        </div>


                                                    </td>
                                                    
                                                </tr>
                                          <?php endforeach; ?>
                                        <?php else : ?>
                                            <?php echo '<div class="alert alert-warning">No se encontraron registros.</div>'; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table> 
                            </div>  <!-- table-responsive -->
                     </div><!-- span12 -->
<!-- 
                    </div> -->
                </div><!-- row-f -->


            </div><!-- page -->

        </div><!-- main-content -->

        
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small ">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>
        <div class="modalesperando" id="modalesperando"></div>
        
        <script src="assets/js/jquery-2.1.4.min.js"></script>

        
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>

        <!--page specific plugin scripts-->

        <!--[if lte IE 8]>
          <script src="assets/js/excanvas.min.js"></script>
        <![endif]-->
        <script src="assets/js/jquery-ui.custom.min.js"></script>
        <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
        
        <script src="assets/js/bootbox.min.js"></script>
        

        <!--ace scripts-->

        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
        <!--datatables js-->

        <script src="assets/js/jquery.dataTables.min.js"></script>
        <script src="assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="assets/js/bootbox.js"></script> 
        <script src="view/js/tipoambiente.js"></script> 
    </body>
</html>
