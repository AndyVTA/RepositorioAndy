<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <title>Registro de Evento</title>
        
         
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/introjs.css">
        
        <script src="assets/js/ace-extra.min.js"></script>
        <script src="assets/js/jquery-1.11.3.min.js"></script> 
        <style type="text/css">
        .modalesperando {
                display:    none;
                position:   fixed;
                z-index:    50;
                top:        0;
                left:       0;
                height:     100%;
                width:      100%;
                background: rgba( 255, 255, 255, .8 ) 
                            url('assets/images/39.GIF') 
                            50% 50% 
                            no-repeat;
            }
        </style>
       
        
    </head>
    <body class="no-skin">
        <?php
        include 'barrasesion.php';
        ?>




        <div class="main-container ace-save-state" id="main-container">
             <script type="text/javascript">
                try{ace.settings.loadState('main-container')}catch(e){}
            </script>
            <?php
    
                    include 'nav-bar.php';
      
            ?>
            <div class="main-content">
                <div class="breadcrumbs" id="breadcrumbs">
                        <ul class="breadcrumb">
                              <li>
                                <i class="icon-wrench home-icon"></i>
                                <a href="index.php?page=login&accion=listarevento">Publicaciones</a>

                                <span class="divider">
                                    <i class="icon-angle-right arrow-icon"></i>
                                </span>
                            </li>
                          
                            <li class="active">Eventos</li>
                        </ul><!--.breadcrumb-->
                    </div>


               <div class="page-content">
                  
                      <div class="row-fluid">
                        <div class="col-xs-12">
                              
                         <h2 class="header smaller lighter blue">Registro Evento</h2>



                         <div class="widget-box">
                             <div class="widget-header">
                                <h4>Datos</h4>
                                
                             </div><!-- widget-header -->
                             <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row-fluid">
                                        <div class="space-6"></div>
                                 
                                        <form  class="form-horizontal " action="index.php?page=publicaciones&accion=insertarevento" method="POST">
                                            <div class="col-sm-12">

                                                <div class="col-sm-12">

                                                   <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Titulo</label>
                                                       <div class="col-sm-10">
                                                            <input type="text" required="required" class="col-xs-10 col-sm-12" name="titulo" id="titulo" value=""  autocomplete="off">
                                                        </div>
                                                    </div>

                                                        <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Descripción</label>
                                                       <div class="col-sm-10">
                                                            <textarea  rows="3" required="required" class="col-xs-10 col-sm-12" name="descrevento" id="descrevento" value="" autocomplete="off"> 
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Fecha</label>
                                                       <div class="col-sm-3">
                                                         <input class="col-xs-10 col-sm-12 form-control date-picker" tabindex="5" type="text"  value="<?php ;echo date("d-m-Y"); ?>" placeholder="d-mm-yyyy" id=" fechaevento" name="fechaevento" data-date-format="d-mm-yyyy" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Hora <i class="fa fa-clock-o bigger-110"></i></label>
                                                            <div class="col-sm-3 bootstrap-timepicker">
                                                                <input id="hora" name="hora" onkeypress="return soloCaracteresHoras(event)" type="text" class="form-control timepicker1" value=""/>
                                                            </div>


                                                      </div>

                                                        <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Categoria</label>
                                                       <div class="col-sm-3">
                                                        <select class="form-control" name="categoriaevento" id="categoriaevento" required>    
                                                                        <option value="">Seleccione...</option>
                                                                        <option value="G">Gastronomia</option>
                                                                        <option value="A">Artes</option>
                                                                        <option value="M">Música</option>
                                                                        <option value="E">Actividades Escolares</option>
                                                                        <option value="O">Otros</option>
                                                                    </select>
                                                       
                                                        </div>


                                                      </div>
                                                        
                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Link Foto</label>
                                                       <div class="col-sm-10">
                                                            <input type="text"  required="required" class="col-xs-10 col-sm-12" name="linkfoto" id="linkfoto"  value="" autocomplete="off">
                                                        </div>


                                                    </div>
 
                                                    <div class="form-group">
                                                        <label class="col-sm-1 control-label no-padding-right" >Imagen</label>   <center><img  src="" id="img1" alt="Debe Ingresar Link" height="180px" width="400px" /></center>
                                                   
                                                    </div>
                                        


                                                </br>  


                                                </div>
                                                
                                       
                                            </div> 
                                           

                                            <div class="space-6"></div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary"> <i class="fa fa-floppy-o icon-on-left bigger-110"></i> Guardar</button>
                                                 <a href="index.php?page=publicaciones&accion=listarevento" class="btn btn-danger"><i class="fa fa-times-circle icon-on-left bigger-110"></i> Cancelar</a>
                                                
                                            </div>

                                        </form>

                                    </div><!-- row-fluid -->
                                    
                                </div><!-- widget-main -->
                                 
                             </div><!-- widget-body -->
                         </div><!-- widget-box -->
                            
            


                        </div><!-- span12 -->

                      </div><!-- row-fluid -->
                     
               </div> <!-- page-content -->


            </div><!-- main-content -->


        </div><!-- main-container -->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small ">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>
        <div class="modalesperando" id="modalesperando"></div>
       <script src="assets/js/jquery-2.1.4.min.js"></script>
       
         <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>

        <!--page specific plugin scripts>

              <div class="col-sm-10 input-group bootstrap-timepicker">
                <input id="td_termino" onkeypress="return soloCaracteresHoras(event)" type="text" class="<?php echo 'horfinal'.$pfechas[$l]->Codigo_Cur.'-'.$pfechas[$l]->Codigo_Gho; ?> form-control timepicker1" value="<?php echo $pfechas[$l]->horafin_rolexa; ?>" /><span class="input-group-addon"><i class="fa fa-clock-o bigger-110"></i></span>
              </div>

      [if lte IE 8]>
          <script src="assets/js/excanvas.min.js"></script>
        <![endif]-->
        <script src="assets/js/jquery-ui.custom.min.js"></script>
        <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
        
        <script src="assets/js/bootbox.min.js"></script>
        

        <!--ace scripts-->

        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
        <script src="assets/js/bootbox.js"></script> 
        <script type="assets/js/bootstrap.datepicker.js"></script>
        <script src="assets/js/bootstrap-datepicker.min.js"></script>
        <script src="assets/js/bootstrap-datepicker.es.js"></script>

        <script src="assets/js/bootstrap-timepicker.min.js"></script>
           <script type="text/javascript">
           
            jQuery(function($) {
                $('.date-picker').datepicker({
                  autoclose: true,
                  language:'es',
                  todayHighlight: true
                });
            });
        </script>
        <script type="text/javascript">
           
            jQuery(function($) {
                $('.date-picker').datepicker({
                  autoclose: true,
                  language:'es',
                  todayHighlight: true
                });

            });
            $('.timepicker1').timepicker({
                    minuteStep: 1,
                     format: 'HH:mm',
                    defaultTime:false,
                    showSeconds: false,
                    showMeridian: false,
                    icons: {
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down'
                    }
            });
     </script>

        <script src="view/js/evento.js"></script>

    </body>
</html>
